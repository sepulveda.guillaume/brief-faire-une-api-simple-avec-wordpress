<?php

/*
Plugin Name: API
*/


// Ajout image mis en avant

add_theme_support('post-thumbnails');


// Création CPT apprenants + catégories promotions + étiquettes compétences

function montheme_register_post_types()
{
    $labels = array(
        'name' => 'apprenants',
        'all_items' => 'Tous les apprenants',  // affiché dans le sous menu
        'singular_name' => 'Apprenant',
        'add_new_item' => 'Ajouter un apprenant',
        'edit_item' => 'Modifier l\'apprenant',
        'menu_name' => 'Apprenants'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => true,
        'has_archive' => true,
        'supports' => array('title', 'thumbnail', 'editor', 'excerpt'),
        'menu_icon' => 'dashicons-admin-customizer',

    );
    register_post_type('apprenants', $args);
}


function montheme_register_taxonomies_types()
{
    $labels = array(
        'name' => 'promotions',
        'all_items' => 'Toutes les promotions',  // affiché dans le sous menu
        'singular_name' => 'Promotion',
        'add_new_item' => 'Ajouter une promotion',
        'edit_item' => 'Modifier la promotion',
        'menu_name' => 'Promotions'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => true,
        'has_archive' => true,
        'supports' => array('title', 'thumbnail'),
        'menu_icon' => 'dashicons-admin-customizer',

    );
    register_taxonomy('promotions', 'apprenants', $args);
}

function montheme_register_etiquettes_types()
{
    $labels = array(
        'name' => 'competences',
        'all_items' => 'Toutes les compétences',  // affiché dans le sous menu
        'singular_name' => 'Compétence',
        'add_new_item' => 'Ajouter une compétence',
        'edit_item' => 'Modifier la compétence',
        'menu_name' => 'Compétences'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => false,
        'has_archive' => true,
        'supports' => array('title', 'thumbnail'),
        'menu_icon' => 'dashicons-admin-customizer',

    );
    register_taxonomy('competences', 'apprenants', $args);
}

add_action('init', 'montheme_register_post_types');
add_action('init', 'montheme_register_taxonomies_types');
add_action('init', 'montheme_register_etiquettes_types');


// Création ACF => métadonnées


function init_metabox()
{
    add_meta_box('url_crea', 'Liens', 'url_crea', 'apprenants');
}

function url_crea($post)
{
    $url_linkedin = get_post_meta($post->ID, '_linkedin_crea', true);
    $url_portfolio = get_post_meta($post->ID, '_portfolio_crea', true);
    $url_cv = get_post_meta($post->ID, '_cv_crea', true);
    $file_cv = get_post_meta($post->ID, '_file_cv_crea', true);
    echo '<label for="url_meta">Lien Linkedin :</label>';
    echo '<input id="url_meta" type="url" name="url_linkedin" value="' . $url_linkedin . '" />';
    echo '<label for="url_meta">Lien Portfolio :</label>';
    echo '<input id="url_meta" type="url" name="url_portfolio" value="' . $url_portfolio . '" />';
    echo '<label for="url_meta">Lien CV :</label>';
    echo '<input id="url_meta" type="url" name="url_cv" value="' . $url_cv . '" />';
    echo '<label for="url_meta">Joindre CV :</label>';
    echo '<input id="url_meta" type="file" name="file_cv" value="' . $file_cv . '" />';
}

function save_metabox($post_id)
{
    update_post_meta($post_id, '_linkedin_crea', esc_url($_POST['url_linkedin']));
    update_post_meta($post_id, '_portfolio_crea', esc_url($_POST['url_portfolio']));
    update_post_meta($post_id, '_cv_crea', esc_url($_POST['url_cv']));
    update_post_meta($post_id, '_file_cv_crea', esc_url($_POST['file_cv']));
}

add_action('add_meta_boxes', 'init_metabox');
add_action('save_post', 'save_metabox');


// Récupération des ACF dans le JSON

function acf_to_rest_api($response, $post, $request)
{
    if (isset($post)) {
    $url_linkedin = get_post_meta($post->ID, '_linkedin_crea');
    $url_portfolio = get_post_meta($post->ID, '_portfolio_crea');
    $url_cv = get_post_meta($post->ID, '_cv_crea');
    $file_cv = get_post_meta($post->ID, '_file_cv_crea');
    $acf = array_merge($url_linkedin, $url_portfolio, $url_cv, $file_cv);
    $response->data['acf'] = $acf;
    }
    return $response;
}
add_filter('rest_prepare_apprenants', 'acf_to_rest_api', 10, 3);


// Base de données WordPress

/* global $wpdb;

$promotion = "promotion2";
$query = $wpdb->prepare("SELECT * FROM ($wpdb->terms) WHERE slug = %s", [$promotion]);
$results = $wpdb->get_results($query);
echo '<pre>';
var_dump($results);
echo '</pre>';
die();     */




// ============= API ============= //

// Pour accéder à l'API, authentification nécessaire

/* add_filter( 'rest_authentication_errors', function( $result ) {
    // If a previous authentication check was applied,
    // pass that result along without modification.
    if ( true === $result || is_wp_error( $result ) ) {
        return $result;
    }
 
    // No authentication has been performed yet.
    // Return an error if user is not logged in.
    if ( ! is_user_logged_in() ) {
        return new WP_Error(
            'rest_not_logged_in',
            __( 'You are not currently logged in.' ),
            array( 'status' => 401 )
        );
    }
 
    // Our custom authentication check should have no effect
    // on logged-in requests
    return $result;
}); */


// Liste de tous les apprenants avec l'ensemble des informations => http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants

function allApprenants()
{
    register_rest_route('wp/v2', '/apprenants/all', [
        'methods' => 'GET',
        'callback' => 'all_promotion_data',
    ]);
}

function all_promotion_data($data)
{
    //definition des arguments du loop 
    $args = [
        'post_type' => 'apprenants',
        'numberposts' => -1,
    ];
    //définition des variables pour la génération du json
    $posts = get_posts($args);
    if (empty($posts)) {
        return null;
    }
    $data = [];
    $i = 0;
    $j = 0;
    foreach ($posts as $post) {
        $data[$i]['nom'] = $post->post_title;
        $data[$i]['extrait'] = $post->post_excerpt;
        $data[$i]['promotion'] = get_the_terms($post->ID, 'promotions')[0]->name;
        $competences = get_the_terms($post->ID, 'competences');
        foreach ($competences as $competence) {
            $data[$i]['compétences'][$j] = $competence->name;
            $j++;
        }   
        $data[$i]['image'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
        $data[$i]['liens']['linkedin'] = get_post_meta($post->ID, '_linkedin_crea');
        $data[$i]['liens']['portfolio'] = get_post_meta($post->ID, '_portfolio_crea');
        $data[$i]['liens']['cv'] = get_post_meta($post->ID, '_cv_crea');
        $data[$i]['fichier']['cv'] = get_post_meta($post->ID, '_file_cv_crea');
        $i++;
    }
    return $data;
}

add_action('rest_api_init', 'allApprenants');


// Liste des apprenants de la P1 année 2021 => http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp-json/wp/v2/apprenants/p1

function p1Apprenants()
{
    register_rest_route('wp/v2', '/apprenants/p1', [
        'methods' => 'GET',
        'callback' => 'p1_promotion_data',
    ]);
}

function p1_promotion_data($data)
{
    //definition des arguments du loop 
    $args = [
        'post_type' => 'apprenants',
        'numberposts' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'promotions',
                'field' => 'name',
                'terms' => 'Promotion 1' 
            )
        )
    ];
    //définition des variables pour la génération du json
    $posts = get_posts($args);

    if (empty($posts)) {
        return null;
    }
    $data = [];
    $i = 0;
    $j = 0;
    foreach ($posts as $post) {
        $data[$i]['nom'] = $post->post_title;
        $data[$i]['extrait'] = $post->post_excerpt;
        $data[$i]['promotion'] = get_the_terms($post->ID, 'promotions')[0]->name;
        $competences = get_the_terms($post->ID, 'competences');
        foreach ($competences as $competence) {
            $data[$i]['compétences'][$j] = $competence->name;
            $j++;
        }   
        $data[$i]['image'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
        $data[$i]['liens']['linkedin'] = get_post_meta($post->ID, '_linkedin_crea');
        $data[$i]['liens']['portfolio'] = get_post_meta($post->ID, '_portfolio_crea');
        $data[$i]['liens']['cv'] = get_post_meta($post->ID, '_cv_crea');
        $data[$i]['fichier']['cv'] = get_post_meta($post->ID, '_file_cv_crea');
        $i++;
    }
    return $data;
}

add_action('rest_api_init', 'p1Apprenants');



// Liste des apprenants de la P2 année 2022 => http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/p2

function p2Apprenants()
{
    register_rest_route('wp/v2', '/apprenants/p2', [
        'methods' => 'GET',
        'callback' => 'p2_promotion_data',
    ]);
}

function p2_promotion_data($data)
{
    //definition des arguments du loop 
    $args = [
        'post_type' => 'apprenants',
        'numberposts' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'promotions',
                'field' => 'name',
                'terms' => 'Promotion 2' 
            )
        )
    ];
    //définition des variables pour la génération du json
    $posts = get_posts($args);

    if (empty($posts)) {
        return null;
    }
    $data = [];
    $i = 0;
    $j = 0;
    foreach ($posts as $post) {
        $data[$i]['nom'] = $post->post_title;
        $data[$i]['extrait'] = $post->post_excerpt;
        $data[$i]['promotion'] = get_the_terms($post->ID, 'promotions')[0]->name;
        $competences = get_the_terms($post->ID, 'competences');
        foreach ($competences as $competence) {
            $data[$i]['compétences'][$j] = $competence->name;
            $j++;
        }   
        $data[$i]['image'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
        $data[$i]['liens']['linkedin'] = get_post_meta($post->ID, '_linkedin_crea');
        $data[$i]['liens']['portfolio'] = get_post_meta($post->ID, '_portfolio_crea');
        $data[$i]['liens']['cv'] = get_post_meta($post->ID, '_cv_crea');
        $data[$i]['fichier']['cv'] = get_post_meta($post->ID, '_file_cv_crea');
        $i++;
    }
    return $data;
}

add_action('rest_api_init', 'p2Apprenants');


// Liste des apprenants front-end => http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/frontend

function frontendApprenants()
{
    register_rest_route('wp/v2', '/apprenants/frontend', [
        'methods' => 'GET',
        'callback' => 'frontend_promotion_data',
    ]);
}

function frontend_promotion_data($data)
{
    //definition des arguments du loop 
    $args = [
        'post_type' => 'apprenants',
        'numberposts' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'competences',
                'field' => 'name',
                'terms' => 'Front-end', 
            )
        )
    ];
    //définition des variables pour la génération du json
    $posts = get_posts($args);

    if (empty($posts)) {
        return null;
    }
    $data = [];
    $i = 0;
    $j = 0;
    foreach ($posts as $post) {
        $data[$i]['nom'] = $post->post_title;
        $data[$i]['extrait'] = $post->post_excerpt;
        $data[$i]['promotion'] = get_the_terms($post->ID, 'promotions')[0]->name;
        $competences = get_the_terms($post->ID, 'competences');
        foreach ($competences as $competence) {
            $data[$i]['compétences'][$j] = $competence->name;
            $j++;
        }   
        $data[$i]['image'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
        $data[$i]['liens']['linkedin'] = get_post_meta($post->ID, '_linkedin_crea');
        $data[$i]['liens']['portfolio'] = get_post_meta($post->ID, '_portfolio_crea');
        $data[$i]['liens']['cv'] = get_post_meta($post->ID, '_cv_crea');
        $data[$i]['fichier']['cv'] = get_post_meta($post->ID, '_file_cv_crea');
        $i++;
    }
    return $data;
}

add_action('rest_api_init', 'frontendApprenants');


// Liste des apprenants back-end => http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/backend

function backendApprenants()
{
    register_rest_route('wp/v2', '/apprenants/backend', [
        'methods' => 'GET',
        'callback' => 'backend_promotion_data',
    ]);
}

function backend_promotion_data($data)
{
    //definition des arguments du loop 
    $args = [
        'post_type' => 'apprenants',
        'numberposts' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'competences',
                'field' => 'name',
                'terms' => 'Back-end', 
            )
        )
    ];
    //définition des variables pour la génération du json
    $posts = get_posts($args);

    if (empty($posts)) {
        return null;
    }
    $data = [];
    $i = 0;
    $j = 0;
    foreach ($posts as $post) {
        $data[$i]['nom'] = $post->post_title;
        $data[$i]['extrait'] = $post->post_excerpt;
        $data[$i]['promotion'] = get_the_terms($post->ID, 'promotions')[0]->name;
        $competences = get_the_terms($post->ID, 'competences');
        foreach ($competences as $competence) {
            $data[$i]['compétences'][$j] = $competence->name;
            $j++;
        }   
        $data[$i]['image'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
        $data[$i]['liens']['linkedin'] = get_post_meta($post->ID, '_linkedin_crea');
        $data[$i]['liens']['portfolio'] = get_post_meta($post->ID, '_portfolio_crea');
        $data[$i]['liens']['cv'] = get_post_meta($post->ID, '_cv_crea');
        $data[$i]['fichier']['cv'] = get_post_meta($post->ID, '_file_cv_crea');
        $i++;
    }
    return $data;
}

add_action('rest_api_init', 'backendApprenants');


// Liste des 16 derniers apprenants => http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/last16

function last16Apprenants()
{
    register_rest_route('wp/v2', '/apprenants/last16', [
        'methods' => 'GET',
        'callback' => 'last16_promotion_data',
    ]);
}

function last16_promotion_data($data)
{
    //definition des arguments du loop 
    $args = [
        'post_type' => 'apprenants',
        'numberposts' => 16,
        'orderby' => 'date',
        'order' => 'DESC'
    ];
    //définition des variables pour la génération du json
    $posts = get_posts($args);

    if (empty($posts)) {
        return null;
    }
    $data = [];
    $i = 0;
    $j = 0;
    foreach ($posts as $post) {
        $data[$i]['nom'] = $post->post_title;
        $data[$i]['extrait'] = $post->post_excerpt;
        $data[$i]['promotion'] = get_the_terms($post->ID, 'promotions')[0]->name;
        $competences = get_the_terms($post->ID, 'competences');
        foreach ($competences as $competence) {
            $data[$i]['compétences'][$j] = $competence->name;
            $j++;
        }   
        $data[$i]['image'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
        $data[$i]['liens']['linkedin'] = get_post_meta($post->ID, '_linkedin_crea');
        $data[$i]['liens']['portfolio'] = get_post_meta($post->ID, '_portfolio_crea');
        $data[$i]['liens']['cv'] = get_post_meta($post->ID, '_cv_crea');
        $data[$i]['fichier']['cv'] = get_post_meta($post->ID, '_file_cv_crea');
        $i++;
    }
    return $data;
}

add_action('rest_api_init', 'last16Apprenants');


