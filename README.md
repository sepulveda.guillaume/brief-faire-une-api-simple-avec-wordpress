**Liste des URL pour utilisation de l'API :**


- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants => accès à toutes les données pour le CPT apprenants sans filtres

- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/all => liste de tous les apprenants avec l'ensemble des informations demandées dans le brief

- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants?promotions={id} => accès aux apprenants selon promotions sans filtres (id = 5 => P1, id = 6 => P2)
- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/p1 => accès aux P1 avec infos brief
- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/p2 => accès aux P2 avec infos brief

- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants?search={mot-clé} => accès aux données d'un ou plusieurs apprenants avec la recherche d'un mot-clé contenu dans le titre, le contenu ou l'extrait

- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants?competences={id} => accès aux apprenants selon compétences sans filtres (id = 9 => Front-end, id = 10 => Back-end)
- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/frontend => accès aux apprenants front-end avec infos brief
- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/backend => accès aux apprenants back-end avec infos brief

- http://localhost:8888/brief-faire-une-api-simple-avec-wordpress/wp-json/wp/v2/apprenants/last16 => liste des 16 derniers apprenants créés dans la base de données avec infos brief


